      PUBLIC median

      SECTION .text : CODE (2)
      THUMB
        

        
median:
      PUSH      {R4-R11}
      MOV       R3,     R0                              ;coloca no R2 o valor de R1, que � o tamanho do vetor

      MOV       R5,     #255                            ;o proximo loop vai copiar o vetor de entrada pro de sa�da, e ja
      MOV       R6,     #0                              ;vai aproveitar pra obter o menor e o maior valor do vetor, 
                                                        ;que serao guardados respectivamente em R5 e R6

cpyi2o:                                                 ;copia o vetor original para o de saida, a ser ordenado
      CBZ       R3,     pigeon_init                       ;depois que copiar, pode comecar a ordenar
      SUB       R3,     R3,     #1                      ;iteracao do loop
      LDRB      R4,     [R1,R3]                         ;carrega o valor,
      CMP       R4,     R5
      IT        LT
        MOVLT   R5,     R4
      CMP       R6,     R4
      IT        LT
        MOVLT   R6,     R4
      STRB      R4,     [R2,R3]                         ;descarrega o valor no outro vetor
      B         cpyi2o                                  ;volta do loop

;R0 guarda o tamanho do vetor
;R1 aponta para o vetor de entrada
;R2 aponta para o vetor de saida
;R3 esta disponivel
;R4 esta disponivel
;R5 guarda o menor valor
;R6 guarda o maior valor
;R7 esta disponivel
;R8 esta disponivel

pigeon_init:
      SUB       R6,     R6,     R5                      ;para fazer a ordenacao pelo 
      ADD       R6,     R6,     #1
      MOV       R3,     R6                              ;principio do pombal, e' necessario
      MOV       R4,     #0                              ;um vetor com zeros, que sera alocado em R7
      
      ADDS      R10,    R6,     #1
      SUBS      R7,     SP,     R10
      
pigeon_init_loop:
      CBZ       R3,     holes_populate     
        SUB     R3,     R3,     #1
        STRB    R4,     [R7,R3]
        B       pigeon_init_loop
          
;R0 guarda o tamanho do vetor
;R1 aponta para o vetor de entrada
;R2 aponta para o vetor de saida
;R3 esta disponivel
;R4 esta disponivel
;R5 guarda o menor valor
;R6 guarda o tamanho do vetor de buracos
;R7 aponta para o vetor de buracos
;R8 esta disponivel

holes_populate:
      MOV       R3,     R0

holes_populate_loop:
      CBZ       R3,     sorting
        SUB     R3,     R3,     #1
        LDRB    R4,     [R2,R3]
        SUB     R4,     R4,     R5
        LDRB    R8,     [R7,R4]
        ADD     R8,     R8,     #1
        STRB    R8,     [R7,R4]
        B       holes_populate_loop
          
;R0 guarda o tamanho do vetor
;R1 aponta para o vetor de entrada
;R2 aponta para o vetor de saida
;R3 esta disponivel
;R4 esta disponivel
;R5 guarda o menor valor
;R6 guarda o tamanho do vetor de buracos
;R7 aponta para o vetor de buracos
;R8 esta disponivel

sorting:
      MOV       R3,     R6
      SUB       r8,     R0,     #1
outerloop: 
      CBZ       R3,     outerloop_end
        SUB     R3,     R3,     #1
        LDRB    r4,     [R7,R3]
  innerloop:
        CBZ     r4,     innerloop_end
          SUB   r4,     r4,     #1
          ADD   R9,     R3,     R5
          STRB  R9,     [R2,r8]
          SUB   r8,     r8,     #1
          B     innerloop
  innerloop_end:
        B outerloop
          
outerloop_end:                                          ;finaliza o loop de ordenacao
        TST     R0,     #1                              ;testa o tamanho do vetor com 1
        BNE     impar                                   ;se for igual, � impar
        B       par                                     ;se nao for igual, � par
        
impar:                                                  ;quando o array tem tamanho impar
        ASR     R0,     R0,     #1                      ;rolando pra direita, ele guarda a posicao desejada de offset
        LDRB    R0,     [R2, R0]                        ;na hora que fizer o offset, guarda no R0 porque ja � a resposta
        B fim                                           
par:                                                    ;quando o array tem tamanho par
        ASR     R0,     R0,     #1                      ;quando pegar a metade do tamanho do vetor, vai ser o offset para
        LDRB    R3,     [R2, R0]                        ;o item depois da metade do vetor, que � guardado
        SUB     R0,     R0,     #1                      ;se decrementar o valor do offset, 
        LDRB    R4,     [R2, R0]                        ;� o valor antes da metade do array, que � guardado
        ADD     R0,     R3,     R4                      ;entao � teita a media entre os valores
        ASR     R0,     R0,     #1                      ;guardados e esta � guardada em R0
        B fim
        
fim:
        POP {R4-R11}
        BX LR
        END
