#ifndef __COUNTER_H__
#define __COUNTER_H__


#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
extern void counterInit(void);
extern uint32_t getCounterValue(void);
extern void resetCounterValue(void);


#ifdef __cplusplus
}
#endif


#endif // __COUNTER_H__