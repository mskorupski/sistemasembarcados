#include "gui.h"

tContext sContext;
char straux[22];


void guiInit (void){
  cfaf128x128x16Init();
  cfaf128x128x16Clear();
  GrContextInit(&sContext, &g_sCfaf128x128x16);
  GrFlush(&sContext);
  GrContextFontSet(&sContext, g_psFontFixed6x8);
  GrContextForegroundSet(&sContext, ClrWhite);
  GrContextBackgroundSet(&sContext, ClrBlack);  
  GrStringDraw(&sContext, "Number of edge counts", -1, 0, 10, true);
  GrStringDraw(&sContext, "_____________________", -1, 0, 60, true);
  GrStringDraw(&sContext, "      Frequency      ", -1, 0, 80, true);
  memset(straux, 0, 22);
}

void guiPrint(uint32_t counts, float frequency){
  sprintf(straux,"      %.3e      ",frequency);
  GrStringDraw(&sContext, straux, -1, 0, 110, true);
  sprintf(straux,"      %05d      ",counts);
  GrStringDraw(&sContext, straux, -1, 0, 40, true);
}