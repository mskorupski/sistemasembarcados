#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "inc/tm4c1294ncpdt.h" // CMSIS-Core
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h" // driverlib
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "system_tm4c1294ncpdt.h" // CMSIS-Core
#include "counter.h"
#include "gui.h"

#define Window 65535
//(time) window size for the timer0

void Timer0IntHandler(void);
void timer0Init(void);

volatile uint32_t counter = 0;
float f = 1.0;

void main(void){
  SystemInit();
  guiInit();
  counterInit();
  timer0Init();

while(1){
    f=SystemCoreClock*1.0/Window*((int64_t)counter);
    //The 32 bits unsigned bit to 64 bits signed typecast is due the signed 
    //nature of floating point numbers, as specified in IEEE754
    guiPrint(counter,f);
  }//main loop
}//main

void Timer0IntHandler(void){
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    // Clear timer0a, allowing it to (re)start, counting until the next timeout
    counter = getCounterValue();
    // Saves the value of the counter in the 'counter' variable
    resetCounterValue();
    // Reset the counter
}//Timer0IntHandler

void timer0Init(void){
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
  //Enable the timer0 peripheral of the microcontroller
  while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0)){}
  //While not ready, it's not a good idea making any changes in timer0 registers
  IntMasterEnable();
  //Enable interrupts in the microcontroller
  TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
  //Configure timer0 to periodic mode
  TimerLoadSet(TIMER0_BASE, TIMER_A, Window);
  ///Sets the timer load value, otherwise, the value would be immediately loaded 
  //into the timer. 
  IntEnable(INT_TIMER0A);
  //Enable the timer0a interrupt 
  TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
  //Sets the timer0a as timeout triggered
  TimerEnable(TIMER0_BASE, TIMER_A);
  //Enable timer0a to start "counting clock"
}//timer0Init