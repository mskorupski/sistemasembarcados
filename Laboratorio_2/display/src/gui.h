#ifndef __GUI_H__
#define __GUI_H__


#ifdef __cplusplus
extern "C"
{
#endif
//The code is entirely in C, but with this define, it's can be compiled in C++ 
//as well.
  
  
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "inc/tm4c1294ncpdt.h" // CMSIS-Core
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h" // driverlib
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "grlib/grlib.h"
#include "cfaf128x128x16.h"

extern void guiInit(void);
extern void guiPrint(uint32_t counts, float frequency);

#ifdef __cplusplus
}
#endif


#endif // __GUI_H__
