#ifndef __COUNTER_H__
#define __COUNTER_H__


#ifdef __cplusplus
extern "C"
{
#endif
//The code is entirely in C, but with this define, it's can be compiled in C++ 
//as well.

  
#include <stdint.h>
extern void counterInit(void);
extern uint32_t getCounterValue(void);
extern void resetCounterValue(void);


#ifdef __cplusplus
}
#endif


#endif // __COUNTER_H__