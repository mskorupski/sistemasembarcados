#include "counter.h"

void counterInit(void){
  *(volatile uint32_t *)0x400FE604 |= 0x00000008;     
//enable a clock to timer 3, by setting the bit/field 3 value of 0x400FE604 
//register (0x400FE000 base + 0x604 offset) to 1 (equivalent to 0x8 OR mask) 
//more information can be found at TM4C1294NCPDT DATA SHEET, page 380
  *(volatile uint32_t *)0x400FE608 |= 0x00000800; 
//enable port M to run a clock, by setting the bit/field 11 value of 0x400FE608 
//register (0x400FE000 base + 0x608 offset) to 1 (equivalent to 0x800 OR mask) 
//more information can be found at TM4C1294NCPDT DATA SHEET, page 382

  *(volatile uint32_t *)0x40063400 &= ~0x00000004;
//turns PM2 into an input pin, by setting the bit/field 2 value of the M-GPIODIR 
//register (0x40063000 base + 0x400 offset) to 0 (equivalent to 0x800 NAND mask) 
//more information can be found at TM4C1294NCPDT DATA SHEET, page 760
  *(volatile uint32_t *)0x4006351C |= 0x00000004; 
//Schmitt-triggers the PM2 pin, by setting the bit/field 2 value of the M-GPIODEN 
//register (0x40063000 base + 0x51C offset) to 1 (equivalent to 0x4 OR mask) 
//more information can be found at TM4C1294NCPDT DATA SHEET, page 781
  *(volatile uint32_t *)0x40063420 |= 0x00000004;
//enable the alternate hardware function of PM2 pin, by setting the bit/field 2 
//value of the M-GPIOAFSEL register (0x40063000 base + 0x420 offset) to 1 
//(equivalent to 0x4 OR mask) 
//more information can be found at TM4C1294NCPDT DATA SHEET, page 770
  *(volatile uint32_t *)0x4006352C &= ~0x00000F00;
//clear the PM2 configuration, by setting the whole 3th nibble of the M-GPIOPCTL
//register (0x40063000 base + 0x52C offset) as 0 (equivalent to 0xF00 NAND mask)
//more information can be found at TM4C1294NCPDT DATA SHEET, page 770  
  *(volatile uint32_t *)0x4006352C |= 0x00000300;
//configure PM2 as the T3CCP0 pin, by setting the 3th nibble of the M-GPIOPCTL
//register (0x40063000 base + 0x52C offset) as 3 (equivalent to 0x300 OR mask)
//more information can be found at TM4C1294NCPDT DATA SHEET, page 743

  *(volatile uint32_t *)0x4003300C &= ~0x00000001;
//disable the subtimer A, by setting the bit/field 0 value of the 3-GPTMCTL 
//register (0x40033000 base + 0x00C offset) to 0 (equivalent to 0x1 NAND mask)
//more information can be found at TM4C1294NCPDT DATA SHEET, page 986
  *(volatile uint32_t *)0x40033000 = 0x00000004;
//selects the 16-bit timer configuration, by setting the bits/fields 0:2 value 
//of the 3-GPTMCFG register (0x40033000 base + 0x000 offset) to 0x4 - other bits
//are reserved and should not be set.
//more information can be found at TM4C1294NCPDT DATA SHEET, page 976
  *(volatile uint32_t *)0x40033004 = 0x00000013; 
//sets the subtimer A to positive count, by setting the bit/field 4 value of the 
//3-GPTMTAMR register (0x40033000 base + 0x004 offset) to 1, as well as the 
//bits/fields 1:0 to 0x3 (to set the timer mode as capture mode) and bit/field 2 
//to 0 (when in capture mode, to be used as an edge counter); these 3 settings, 
//when combined, results in 0b1XXX|0x3|0b0XX = 0x13 - other bits are reserved or 
//used by other timer functions, and should not be set.
//more information can be found at TM4C1294NCPDT DATA SHEET, page 977
  *(volatile uint32_t *)0x40033030 = 0x0FFFFFFF;
//defines the timer A count limit (i.e. how many edge events are counted); a 
//match event happens when GPTMTAR is equal to GPTMTAMATCHR register (0x40033000 
//base + 0x030 offset). 
//more information can be found at TM4C1294NCPDT DATA SHEET, page 1006
  *(volatile uint32_t *)0x40033040 = 0x000000FF;
//extend the range of the GPTMTAMATCHR register, by setting the bits/fields 7:0 
//value of the 3-GPTMTAPMRregister (0x40033000 base + 0x040 offset) as a 8 bit 
//number - other bits are reserved and should not be set.
//more information can be found at TM4C1294NCPDT DATA SHEET, page 1010
  
  *(volatile uint32_t *)0x4003300C &= ~0x0000000C;
//set the timer A event mode as positive (rising) edge detection, by setting the 
//bits/fields 3:2 value of the 3-GPTMCTL register (0x40033000 base + 0x00C 
//offset) to 0 (equivalent to 0xC NAND mask)
//more information can be found at TM4C1294NCPDT DATA SHEET, page 986
  *(volatile uint32_t *)0x4003300C |= 0x00000001;
//enable the subtimer A, by setting the bit/field 0 value of the 3-GPTMCTL 
//register (0x40033000 base + 0x00C offset) to 1 (equivalent to 0x1 OR mask)
//more information can be found at TM4C1294NCPDT DATA SHEET, page 986
  
  return;
}

uint32_t getCounterValue(void){
  return *(volatile uint32_t *)0x40033048;
//returns the current value of subtimer A, in 3-GPTMTAR register 
//(0x40033000 base + 0x048 offset)
//more information can be found at TM4C1294NCPDT DATA SHEET, page 1012
}

void resetCounterValue(void){
  *(volatile uint32_t *)0x40033050 = 0x00000000;
//resets the counter (set subtimer A value to 0), by setting the running value 
//of the 3-GPTMTAV regiter (0x40033000 base + 0x050 offset) value to 0x0
//more information can be found at TM4C1294NCPDT DATA SHEET, page 1014
  return;
}